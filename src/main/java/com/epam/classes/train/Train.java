package src.main.java.com.epam.classes.train;

import java.util.Objects;
import java.time.LocalTime;

public class Train {
    private String destination;
    private int number;
    // decided to use LocalTime from the java.time package to make it easier
    private LocalTime departureTime;

    public Train(String destination, int number, LocalTime departureTime) {
        this.destination = destination;
        this.number = number;
        this.departureTime = departureTime;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public LocalTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalTime departureTime) {
        this.departureTime = departureTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Train train = (Train) o;

        if (number != train.number) {
            return false;
        }
        if (!Objects.equals(destination, train.destination)) {
            return false;
        }
        return Objects.equals(departureTime, train.departureTime);
    }

    @Override
    public int hashCode() {
        int result = destination != null ? destination.hashCode() : 0;
        result = 31 * result + number;
        result = 31 * result + (departureTime != null ? departureTime.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Train{" +
                "destination='" + destination + '\'' +
                ", number=" + number +
                ", departureTime=" + departureTime +
                '}';
    }
}
