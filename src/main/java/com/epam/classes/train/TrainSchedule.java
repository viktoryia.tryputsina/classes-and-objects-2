package src.main.java.com.epam.classes.train;

public class TrainSchedule {
    private Train[] trains;

    public TrainSchedule(Train[] trains) {
        this.trains = trains;
    }

    public void sortByNumber() {
        for (int i = 0; i < trains.length - 1; i++) {
            for (int j = 0; j < trains.length - 1 - i; j++) {
                if (trains[j].getNumber() > trains[j + 1].getNumber()) {
                    Train temp = trains[j + 1];
                    trains[j + 1] = trains[j];
                    trains[j] = temp;
                }
            }
        }
    }

    public void sortByDestination() {
        for (int i = 0; i < trains.length - 1; i++) {
            for (int j = 0; j < trains.length - 1 - i; j++) {
                int isGreater = trains[j].getDestination().compareTo(trains[j + 1].getDestination());
                boolean isEarlier = trains[j].getDepartureTime().isAfter(trains[j + 1].getDepartureTime());
                if (isGreater > 0 || (isGreater == 0 && isEarlier)) {
                    Train temp = trains[j + 1];
                    trains[j + 1] = trains[j];
                    trains[j] = temp;
                }
            }
        }
    }

    public Train getTrainByNumber(int number){
        Train result = null;
        for (Train train: trains) {
            if(train.getNumber() == number){
                result = train;
            }
        }

        return result;
    }

    public Train[] getTrains() {
        return trains;
    }
}
