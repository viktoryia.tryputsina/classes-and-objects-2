package src.main.java.com.epam.classes.train;

public class TrainPrinter {
    public void print(TrainSchedule schedule){
        print(schedule.getTrains());
    }
    public void print(Train[] trains) {
        for (Train train : trains) {
            print(train);
        }
    }

    public void print(Train train) {
        System.out.println(train.toString());
    }
}
