package src.main.java.com.epam.classes.student;

import java.util.Arrays;
import java.util.Objects;

public class Student {
    private String surname;
    private String name;
    private int group;
    private int[] grades;

    public Student(String surname, String name, int group, int[] grades) {
        this.surname = surname;
        this.name = name;
        this.group = group;
        this.grades = grades;
    }

    public boolean isExcellentStudent(){
        for(int i = 0; i < grades.length; i++){
            if (grades[i] < 9){
                return false;
            }
        }

        return true;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Student student = (Student) o;

        if (group != student.group) {
            return false;
        }
        if (!Objects.equals(surname, student.surname)) {
            return false;
        }
        if (!Objects.equals(name, student.name)) {
            return false;
        }
        return Arrays.equals(grades, student.grades);
    }

    @Override
    public int hashCode() {
        int result = surname != null ? surname.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + group;
        result = 31 * result + Arrays.hashCode(grades);
        return result;
    }

    @Override
    public String toString() {
        return "Student{" +
                "surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", group=" + group +
                ", grades=" + Arrays.toString(grades) +
                '}';
    }
}
