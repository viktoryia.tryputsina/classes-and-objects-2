package src.main.java.com.epam.classes;


import src.main.java.com.epam.classes.book.Book;
import src.main.java.com.epam.classes.book.BookPrinter;
import src.main.java.com.epam.classes.book.BookRepository;
import src.main.java.com.epam.classes.student.Student;
import src.main.java.com.epam.classes.student.StudentList;
import src.main.java.com.epam.classes.student.StudentPrinter;
import src.main.java.com.epam.classes.train.Train;
import src.main.java.com.epam.classes.train.TrainPrinter;
import src.main.java.com.epam.classes.train.TrainSchedule;

import java.time.LocalTime;

public class Main {

    public static void main(String[] args) {
        // just a small disclaimer: I decided to make all parts with lists to make like in Book task, it is much more convenient to test and understand
        testStudents();
        testBooks();
        testTrains();

    }

    public static void testBooks() {
        Book java = new Book(1, "Java", new String[]{"Gosling"}, "Piter", 2010, 800, 60, "hard");
        Book sharp = new Book(2, "C#", new String[]{"Shildt"}, "Piter", 2009, 750, 55, "hard");
        Book js = new Book(3, "YDKJS", new String[]{"Simpson"}, "Piter", 2022, 192, 40, "soft");
        Book html = new Book(4, "Learning HTML, XHTML и CSS", new String[]{"Simpson"}, "Piter", 2022, 192, 40, "soft");
        Book ruby = new Book(5, "Head First Ruby", new String[]{"McGavren"}, "Piter", 2016, 544, 60, "hard");
        Book cPlusPlus = new Book(6, "Effective Modern C++", new String[]{"Meyers"}, "Вильямс", 2019, 304, 35, "hard");
        Book thePragmaticProgrammer = new Book(7, "The Pragmatic Programmer", new String[]{"Thomas", "Hunt"}, "Вильямс", 2020, 386, 54, "soft");
        Book theSelfTaughtProgrammer = new Book(8, "#The Self-Taught Programmer", new String[]{"Althoff"}, "Эксмо", 2021, 208, 64, "hard");
        Book python = new Book(9, "Python Programming for Beginners", new String[]{"Foster"}, "Эксмо", 2022, 208, 38, "hard");
        Book react = new Book(10, "Learning React: Modern Patterns for Developing React Apps", new String[]{"Banks", "Porsello"}, "Piter", 2022, 320, 48, "soft");


        BookRepository repository = new BookRepository(new Book[]{java, sharp, js, html, ruby, cPlusPlus, thePragmaticProgrammer, theSelfTaughtProgrammer, python, react});
        BookPrinter printer = new BookPrinter();

        Book[] goslingBooks = repository.findByAuthor("Gosling");
        printer.print(goslingBooks);

        Book[] booksByYear = repository.findByYear(2009);
        printer.print(booksByYear);

        Book[] piterBooks = repository.findByPublisher("Piter");
        printer.print(piterBooks);
    }

    public static void testStudents() {
        int[] goodStudentGrades = new int[]{9, 10, 9, 9, 10};
        int[] badStudentGrades = new int[]{4, 5, 4, 5, 4};
        Student[] students = new Student[]{
                new Student("Gosling", "Ryan", 1, goodStudentGrades),
                new Student("Reynolds", "Ryan", 2, goodStudentGrades),
                new Student("Jovovich", "Mila", 3, badStudentGrades),
                new Student("Hathaway", "Ann", 1, goodStudentGrades),
                new Student("Riders", "Winona", 3, badStudentGrades),
                new Student("Cumberbatch", "Benedict", 2, goodStudentGrades),
                new Student("Hiddleston", "Tom", 3, badStudentGrades),
                new Student("Downey", "Robert Jr.", 1, badStudentGrades),
                new Student("Roberts", "Thomas", 2, badStudentGrades),
                new Student("Johansson", "Scarlett", 1, goodStudentGrades)
        };

        StudentList studentList = new StudentList(students);
        StudentPrinter printer = new StudentPrinter();

        Student[] excellentStudents = studentList.findExcellentStudents();
        printer.print(excellentStudents);
    }

    public static void testTrains() {
        Train[] trains = new Train[]{
                new Train("Minsk", 5, LocalTime.of(13, 45)),
                new Train("Vitebsk", 2, LocalTime.of(11, 01)),
                new Train("Mozyr", 7, LocalTime.of(01, 06)),
                new Train("Baranovichy", 1, LocalTime.of(20, 45)),
                new Train("Brest", 3, LocalTime.of(8, 34)),
                new Train("Minsk", 4, LocalTime.of(14, 31)),
                new Train("Vitebsk", 9, LocalTime.of(8, 20)),
                new Train("Mozyr", 8, LocalTime.of(7, 18)),
                new Train("Baranovichy", 6, LocalTime.of(0, 30)),
                new Train("Brest", 10, LocalTime.of(16, 15))
        };
        TrainSchedule schedule = new TrainSchedule(trains);
        TrainPrinter printer = new TrainPrinter();

        Train train1 = schedule.getTrainByNumber(2);
        printer.print(train1);
        System.out.println();

        schedule.sortByNumber();
        printer.print(schedule);
        System.out.println();

        schedule.sortByDestination();
        printer.print(schedule);
        System.out.println();

    }

}
