package src.main.java.com.epam.classes.book;

public class BookRepository {
    private Book[] books;

    public BookRepository(Book[] books) {
        this.books = books;
    }

    public Book[] findByAuthor(String author) {
        Book[] result = new Book[books.length];
        int count = 0;
        for (int i = 0; i < books.length; i++) {
            Book book = books[i];
            if(book != null && contains(book.getAuthors(), author)){
                result[count++] = book;
            }
        }

        Book[] destination = extractSubArray(result, count);

        return destination;
    }

    private boolean contains(String[] items, String target){
        for (String item: items) {
            if(item != null && item.equals(target)){
                return true;
            }
        }
        return false;
    }

    public Book[] findByPublisher(String publisher) {
        Book[] result = new Book[books.length];
        int count = 0;
        for (int i = 0; i < books.length; i++) {
            Book book = books[i];
            if(book != null && book.getPublisher() != null && book.getPublisher().equals(publisher)){
                result[count++] = book;
            }
        }

        Book[] destination = extractSubArray(result, count);

        return destination;
    }

    public Book[] findByYear(int year) {
        Book[] result = new Book[books.length];
        int count = 0;
        for (int i = 0; i < books.length; i++) {
            Book book = books[i];
            if(book != null && book.getYear() == year){
                result[count++] = book;
            }
        }

        Book[] destination = extractSubArray(result, count);

        return destination;
    }

    private Book[] extractSubArray(Book[] source, int count) {
        Book[] destination = new Book[count];
        System.arraycopy(source, 0, destination, 0, count);
        return destination;
    }


}
